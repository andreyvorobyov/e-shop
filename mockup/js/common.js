//$(function(){
$(document).ready(function(){
	
	//click by link and set window status
	//$(".catalog-menu .menu-item").click(function() {	
		//location.href = $(this).attr('data-href');
	//});
	
	
	
	//tabs initial
	$(".tabs").lightTabs();
	//zoom initial
	$('a.zoom').easyZoom();
	
	//popup-cart
	$('#popup-cart').hide();
  	$('#hide-layout').hide();
  	$('#hide-layout').css({opacity: .5});
  				
  	alignCenter($('#popup-cart'));
  				
  	$(window).resize(function() {
		alignCenter($('#popup-cart'));
  	})
  				
  	$('.good .button-buy, #click-cart').click(function() {
		$('#hide-layout, #popup-cart').fadeIn(300);
  	})
  				
  	$('#popup-cart-goto-cart').click(function() {
		$('#hide-layout, #popup-cart').fadeOut(300);
  	})
  				
  	$('#popup-cart-continue-buy').click(function() {
    	$('#hide-layout, #popup-cart').fadeOut(500);
  	})
  				
  	function alignCenter(elem) {
   	elem.css({
      	left: ($('#hide-layout').width() - elem.width()) / 2 + 'px', 
      	top: ($('#hide-layout').height() - elem.height()) / 2 + 'px' 
    	})
  	}
	
	
	$("#popup-cart-goto-cart").click(function() {	
		location.href = $(this).attr('data-href');
	});
	
  	//slider
	$(window).load(function() {
		$('#featured').orbit({
			'bullets': true,
			'timer' : true
			//'animation' : 'horizontal-slide'
		});
	});
	
  	//goods-catalog
   $(".good").mouseenter(function(){
		$(this).find("div.buy-container div.button-buy").css("display","block");    			
    })
    
    $(".good").mouseleave(function(){
		$(this).find("div.buy-container div.button-buy").css("display","none");    			
    })

    //showing sub-category-menu
	$(".catalog-menu .menu-item").click(function(){
		var arrow_left = "img/arrow-left.png";
		var arrow_down = "img/arrow-down.png";
		
		var arrow_mode = $(this).find(".active img").attr("src");
		
		if(arrow_mode == arrow_left){
			$(this).find(".active img").attr('src', arrow_down);
			$(this).next(".sub-menu").slideDown(500);
		}else{//arrow_down
			$(this).find(".active img").attr('src', arrow_left);
			$(this).next(".sub-menu").slideUp(500);
		}    
	});

    //mark sub-category-menu as active
    $(".sub-menu-item").mouseenter(function(){
        $(this).find("div.sub-active").css("background","#913f98");
    })

    $(".sub-menu-item").mouseleave(function(){
        $(this).find("div.sub-active").css("background","#a5cf3a");
    })
	
	//change count
	$('#bt-plus').click(function(){
		var $button = $(this);
		var oldValue = $button.parent().find("input").val();
		$button.parent().find("input").val(parseFloat(oldValue) + 1);
	})
	
	$('#bt-minus').click(function(){
		var $button = $(this);
		var oldValue = $button.parent().find("input").val();
		if (oldValue > 1) {
			var newVal = parseFloat(oldValue) - 1;
		} else {
			newVal = 1;
		}
			$button.parent().find("input").val(newVal);
	})	
		    
   //image carusel
   $('#slider-images-vertical').Thumbelina({
   	orientation:'vertical',         
      $bwdBut:$('#slider-images-vertical .top'),     
      $fwdBut:$('#slider-images-vertical .bottom')
     }) 
     
   //image carusel select image  
   $('#slider-images-vertical ul li img').click(function() { 
		var src = $(this).attr("src").match(/[^\.]+/) + ".jpg";
		$('.zoom-section img').attr('src', src);
        $('.zoom-section a').attr('href', src);
        $('a.zoom').easyZoom();
	})
  						
})