
import json, uuid
from decimal import Decimal
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.db.models import Q
from django.core import serializers
from django.http import Http404
from .models import Product, CatalogTop, CatalogSub, Cart


def get_session_data(session):
    user_id=session.get('user_id', None)
    if not user_id:
        session['user_id'] = str(uuid.uuid1())
    session.modified = True
    return {'user_id': session['user_id']}


def get_paginator_builder(_count, _current):

    MAX_SIZE=9

    result=[]
    if _count > MAX_SIZE:
        c_range=[_current]
        l_sp=False
        r_sp=False
        if _current > 5:
            l_sp=True
            l_range=[1]
            l_range.append('.')
        else:
            l_range=range(1,_current)

        if (_count-_current) >= 5:
            r_sp=True
            r_range=['.']
            r_range.append(_count)
        else:
            r_range=range(_current+1, _count+1)

        if l_sp and r_sp:
            l_range.append(_current-2)
            l_range.append(_current-1)
            r_range.insert(0,_current+1)
            r_range.insert(0,_current+2)
        elif not l_sp and r_sp:
            r_=range(_current+1,_current+1+MAX_SIZE-(len(l_range)+len(c_range)+len(r_range)))
            r_.extend(r_range)
            r_range=r_
        elif not r_sp and l_sp:
            l_=range(_current-(MAX_SIZE-(len(l_range)+len(c_range)+len(r_range))), _current)
            l_range.extend(l_)

        result.extend(l_range)
        result.extend(c_range)
        result.extend(r_range)
    else:
        result=range(1,_count+1)

    return result


def get_fields_from_json(json_data, fields):
    dict = {}
    for i in fields:
        try:
            dict[i] = json_data[i]
        except KeyError:
            dict[i] = None
    return dict


def get_cart_preview(cart):
    dict = {'count':0, 'summ_total':0, 'empty': True}
    for c in cart:
        dict['count'] = dict['count'] + c.count
        dict['summ_total'] = dict['summ_total'] + c.summ

    dict['summ_total'] = '{:.0f}'.format(dict['summ_total'])
    dict['empty'] = dict['count'] == 0
    return dict


def create_template_args(request):
    session_data = get_session_data(request.session)
    cart = Cart.filters.user_id(session_data['user_id'])
    args = {'cart_preview': get_cart_preview(cart),
            'session_data': session_data,}
    return args


#views

def home(request):
    args = create_template_args(request)
    args['product_list'] = Product.objects.all()
    args['catalog_menu'] = CatalogTop.objects.all()
    return render(request, 'home.html', args)


def product_detail(request, slug):
    args = create_template_args(request)
    product = get_object_or_404(Product, slug=slug)
    args['product_detail'] = product
    args['product_in_cart'] = Cart.objects.filter(Q(user_id = args['session_data']['user_id']) & Q(product = product))
    return render(request, 'product_detail.html', args)


def product_catalog(request, slug):
    sub_catalog = get_object_or_404(CatalogSub, slug=slug)
    product_list = Product.objects.filter(catalog = sub_catalog)

    paginator = Paginator(product_list, 5)
    page = request.GET.get('page')
    try:
        product_list = paginator.page(page)
    except PageNotAnInteger:
        #If page not an integer, deliver first page.
        product_list = paginator.page(1)
    except EmptyPage:
        #If page out of range, deliver last page.
        product_list = paginator.page(paginator.num_pages)
    paginator_builder = get_paginator_builder(product_list.paginator.num_pages, product_list.number)

    args = create_template_args(request)
    args['product_list'] = product_list
    args['catalog_menu'] = CatalogTop.objects.all()
    args['sub_catalog'] = sub_catalog
    args['paginator_builder'] = paginator_builder
    return render(request, 'product_catalog.html', args)


def cart(request):
    session_data = get_session_data(request.session)
    args = create_template_args(request)
    args['cart'] = []
    for cart in Cart.filters.user_id(session_data['user_id']):
        args['cart'].append({
            'summ' : '{:.0f}'.format(cart.product.price * cart.count),
            'price' :'{:.0f}'.format(cart.price),
            'count' :cart.count,
            'product_id' : cart.product.id,
            'title' : cart.product.title,
            'product' : cart.product,
        })
    return render(request, 'cart.html', args)


@csrf_exempt
def add_to_cart(request):
    session_data=get_session_data(request.session)
    if request.method == 'POST':
        json_data = get_fields_from_json(json.loads(request.body),['product_id','count'])
        product = get_object_or_404(Product, id=json_data['product_id'])

        if product and session_data['user_id'] and json_data['count']:
            carts = Cart.objects.filter(Q(user_id = session_data['user_id']) & Q(product = product))
            if carts:
                cart = carts[0]
            else:
                cart = Cart()

            cart.product = product
            cart.count = cart.count + int(json_data['count'])
            cart.price = product.price
            cart.user_id = session_data['user_id']
            cart.save()
        else:
            raise Http404 #error 404

    json_text = json.dumps(create_template_args(request)['cart_preview'])
    response = HttpResponse()
    response['Content-Type'] = 'application/json; charset=utf-8'
    response.write(json_text)
    return response


@csrf_exempt
def remove_from_cart(request):
    session_data=get_session_data(request.session)
    if request.method == 'POST':
        json_data = get_fields_from_json(json.loads(request.body),['product_id'])
        product = get_object_or_404(Product, id=json_data['product_id'])

        if product and session_data['user_id']:
            carts = Cart.objects.filter(Q(user_id = session_data['user_id']) & Q(product = product))
            if carts:
                carts[0].delete()
            else:
                raise Http404 #error 404
        else:
            raise Http404 # error 404

    json_text = json.dumps(create_template_args(request)['cart_preview'])
    response = HttpResponse()
    response['Content-Type'] = 'application/json; charset=utf-8'
    response.write(json_text)
    return response


@csrf_exempt
def change_count_in_cart(request):
    session_data = get_session_data(request.session)

    if request.method == 'POST':
        json_data = get_fields_from_json(json.loads(request.body),['product_id', 'count'])
        product = get_object_or_404(Product, id=json_data['product_id'])

        if product and session_data['user_id'] and json_data['count']:
            carts = Cart.objects.filter(Q(user_id=session_data['user_id']) & Q(product=product))
            if carts:
                carts[0].count = json_data['count']
                carts[0].save()
            else:
                raise Http404 #error 404
        else:
            raise Http404 #error 404

    args = create_template_args(request)
    args['cart_preview']['summ'] = '{:.0f}'.format(product.price * json_data['count'])
    args['cart_preview']['price'] = product.get_formated_price()
    args['cart_preview']['product_id'] = product.id
    args['cart_preview']['title'] = product.title
    json_text = json.dumps(args['cart_preview'])
    response = HttpResponse()
    response['Content-Type'] = 'application/json; charset=utf-8'
    response.write(json_text)
    return response


def search(request):
    if request.method == 'GET':
        term = request.GET.get('q')
        product_list = Product.objects.filter(Q(title__contains = term) | Q(description__contains = term))

        paginator = Paginator(product_list, 5)
        page = request.GET.get('page')
        try:
            product_list = paginator.page(page)
        except PageNotAnInteger:
            # If page not an integer, deliver first page.
            product_list = paginator.page(1)
        except EmptyPage:
            # If page out of range, deliver last page.
            product_list = paginator.page(paginator.num_pages)
        paginator_builder = get_paginator_builder(product_list.paginator.num_pages, product_list.number)

        args = create_template_args(request)
        args['product_list'] = product_list
        args['catalog_menu'] = CatalogTop.objects.all()
        args['paginator_builder'] = paginator_builder
        args['term'] = term
        return render(request, 'search.html', args)
    else:
        raise Http404 #error 404


def error404(request):
    return render(request, 'error404.html')

