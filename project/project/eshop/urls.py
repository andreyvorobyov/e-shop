from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^product/(?P<slug>[-\w]+)/$', views.product_detail, name='product-detail'),
    url(r'^catalog/(?P<slug>[-\w]+)/$', views.product_catalog, name='product-catalog'),
    url(r'^search/',views.search),
    url(r'^cart/add-to-cart/$', views.add_to_cart),
    url(r'^cart/remove-from-cart/$', views.remove_from_cart),
    url(r'^cart/change-count-in-cart/$', views.change_count_in_cart),
    url(r'^cart/$', views.cart),
]

handler404 = views.error404