from __future__ import unicode_literals

from django.db import models
from django.contrib import admin
from django.urls import reverse
from .fields import ThumbnailImageField


class CatalogTop(models.Model):
    title = models.CharField(max_length=50)
    order = models.IntegerField()
    slug = models.SlugField()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return ''

    class Meta:
        ordering = ['order']


class CatalogSub(models.Model):
    title = models.CharField(max_length=50)
    order = models.IntegerField()
    slug = models.SlugField()
    parent = models.ForeignKey(CatalogTop)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('product-catalog', args=(self.slug,))

    class Meta:
        ordering = ['order']


class CatalogSubInLine(admin.StackedInline):
    model = CatalogSub
    prepopulated_fields = {'slug': ('title',)}


class CatalogSubAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


class CatalogTopAdmin(admin.ModelAdmin):
    inlines = [CatalogSubInLine]
    prepopulated_fields = {'slug': ('title',)}


class Product(models.Model):
    title = models.CharField(max_length=250, blank=False)
    slug = models.SlugField()
    code = models.CharField(max_length=10)
    price = models.DecimalField(max_digits=18, decimal_places=2)
    catalog = models.ForeignKey(CatalogSub)
    description = models.TextField()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('product-detail', args=(self.slug,))

    def get_formated_price(self):
        return '{:.0f}'.format(self.price)

class ProductPhoto(models.Model):
    product = models.ForeignKey(Product)
    image = ThumbnailImageField(
        upload_to='product_photos',
        thumb_width=200,
        thumb_height=180
    )
    order = models.IntegerField()

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return str(self.id)

    def get_absolute_url(selfs):
        return 'product-photo'  # reverse('product-photo', args=(selfs.id,))


class ProductPhotoInLine(admin.StackedInline):
    model = ProductPhoto


class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    inlines = [ProductPhotoInLine]


class FilterManager(models.Manager):
    def user_id(self, id):
        return super(FilterManager, self).get_queryset().filter(user_id=id)


class Cart(models.Model):
    user_id = models.CharField(max_length=100)
    product = models.ForeignKey(Product)
    count = models.IntegerField(default=0)
    price = models.DecimalField(default=0, max_digits=18, decimal_places=2)
    summ =  models.DecimalField(default=0, max_digits=18, decimal_places=2)

    filters = FilterManager()
    objects = models.Manager()

    def save(self, *args, **kwargs):
        self.summ = self.price * self.count
        super(Cart, self).save(*args, **kwargs)

admin.site.register(Cart)
admin.site.register(Product, ProductAdmin)
admin.site.register(CatalogTop, CatalogTopAdmin)
admin.site.register(CatalogSub, CatalogSubAdmin)
