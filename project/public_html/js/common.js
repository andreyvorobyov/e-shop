function createJSON(product_id, count){
    data = {}
    data['product_id'] = product_id;
    data['count'] = count;
    return JSON.stringify(data);
}

function update_cart_view(data){
    //cart in header
    if (data.empty){
        $("#cart-empty").show();
        $("#cart-full").hide()
    }
    else{
        $("#cart-empty").hide();
        $("#cart-full").show();
        $("#cart-count").html(''+data.count+' товаров');
        $("#cart-summ").html('сумма: '+data.summ_total+' грн.');
    }
    //cart in page
    summ_total = $('#b_summ_total');
    if (summ_total.length != 0){
        summ_total.html("Всего к оплате: "+data.summ_total+" грн");
        $("#cart-product"+data.product_id).find(".total").html(data.summ);
    }
}

function add_to_cart(product_id, count){
    var postUrl = '/cart/add-to-cart/';
    var dataToPost = createJSON(product_id, count);

    $.ajax({
        type: "POST",
        url : postUrl,
        data: dataToPost,
        contentType: "application/json; charset=utf-8"
    }).done(function(data){
        update_cart_view(data);
    }).fail(function(){
        alert("Sorry. Server unavailable. ");
    });
}

function change_count_in_cart(product_id, count){
    var postUrl = '/cart/change-count-in-cart/';
    var dataToPost = createJSON(product_id, count);

    $.ajax({
        type: "POST",
        url : postUrl,
        data: dataToPost,
        contentType: "application/json; charset=utf-8"
    }).done(function(data){
        update_cart_view(data);
        //summ.html(data.summ);
        //summ_total.html("Всего к оплате: "+data.summ_total+" грн");
    }).fail(function(){
        alert("Sorry. Server unavailable. ");
    });
}

function remove_from_cart(product_id){
    var postUrl = '/cart/remove-from-cart/';
    var dataToPost = createJSON(product_id);

    $.ajax({
        type: "POST",
        url : postUrl,
        data: dataToPost,
        contentType: "application/json; charset=utf-8"
    }).done(function(data){
        update_cart_view(data);
    }).fail(function(){
        alert("Sorry. Server unavailable. ");
    });
}

//$(function(){
$(document).ready(function(){

        //tabs initial
        $(".tabs").lightTabs();
        //zoom initial
        $('a.zoom').easyZoom();

        //search
        $('#search-button').click(function(){
            $('#search-form').submit();
        })

        //cart
        $('.cart-button-delete').click(function(){
            product_id = $(this).attr('id').replace('cart-delete', '')
            $('#cart-product'+product_id).remove();
            remove_from_cart(product_id);
        })

        $('.cart-row-product-link').click(function(){
            location.href = $(this).attr('data-href');
        })

        //cart-item
        $('.cart-item-table').click(function(){
            location.href = $(this).attr('data-href');
        })

        //popup-cart
        $('#popup-cart').hide();
        $('#hide-layout').hide();
        $('#hide-layout').css({opacity: .5});

        alignCenter($('#popup-cart'));

        $(window).resize(function() {
            alignCenter($('#popup-cart'));
        })

        $('.good .button-buy, #click-cart').click(function() {

           mode = 0;
           count = 1;
           top_element1 = $(this).closest('.good');
           top_element2 = $(this).closest('.good-element');
           if (top_element1.length != 0){mode = 1}
           else if(top_element2.length != 0){mode = 2}

           if (mode == 1){
                product_id = top_element1.attr('id');
                product_description = $('#product_description_'+product_id).text();
                product_image = $('#product_image_'+product_id).attr("src");
           }else if(mode == 2){
                product_id = top_element2.attr('id');
                product_description = $('#product-title').text();
                product_image = $('#product-title').attr('data-href');
                count = top_element2.find('.input').val();
           }

           add_to_cart(product_id, count);

           //open popup cart
           $('#popup-cart-product').html(product_description);
           $('#popup-cart-img').attr('src', product_image);
           $('#hide-layout, #popup-cart').fadeIn(300);
        })

        $('#popup-cart-goto-cart').click(function() {
            $('#hide-layout, #popup-cart').fadeOut(300);
            location.href = $(this).attr('data-href');
        })

        $('#popup-cart-continue-buy').click(function() {
            $('#hide-layout, #popup-cart').fadeOut(500);
        })
        
        $('#hide-layout').click(function(){
            $('#hide-layout, #popup-cart').fadeOut(500);
        })

        function alignCenter(elem) {
            elem.css({
                left: ($('#hide-layout').width() - elem.width()) / 2 + 'px', 
                top: ($('#hide-layout').height() - elem.height()) / 2 + 'px' 
            })
        }

        //category path
        $('.path-home, .path-item').click(function(){
            data_href = $(this).attr('data-href');
            if(data_href){
                location.href = data_href;
            }
        })

        //slider
        $(window).load(function() {
            $('#featured').orbit({
                'bullets': true,
                'timer' : true
                //'animation' : 'horizontal-slide'
            });
        });

    //goods-catalog
    $(".good").mouseenter(function(){
        $(this).find("div.buy-container div.button-buy").css("display","block");
    })

    $(".good").mouseleave(function(){
        $(this).find("div.buy-container div.button-buy").css("display","none");
    })

    //showing sub-category-menu
    $(".catalog-menu .menu-item").click(function(){
        var arrow_left = "/img/arrow-left.png";
        var arrow_down = "/img/arrow-down.png";

        var arrow_mode = $(this).find(".active img").attr("src");

        if(arrow_mode == arrow_left){
            $(this).find(".active img").attr('src', arrow_down);
            $(this).next(".sub-menu").slideDown(500);
        }else{//arrow_down
            $(this).find(".active img").attr('src', arrow_left);
            $(this).next(".sub-menu").slideUp(500);
        }
    });

    //mark sub-category-menu as active
    $(".sub-menu-item").mouseenter(function(){
        $(this).find("div.sub-active").css("background","#913f98");
    })

    $(".sub-menu-item").mouseleave(function(){
        $(this).find("div.sub-active").css("background","#a5cf3a");
    })

    //change count
    $('.plus').click(function(){
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        var newValue = parseFloat(oldValue) + 1;
        $button.parent().find("input").val(newValue);
        //update summ and summ_total
        cart_row = $(this).closest('.cart-row');
        if (cart_row.length != 0){
            product_id = cart_row.attr('id').replace('cart-product', '');
            change_count_in_cart(product_id, newValue);
        }
    })

    $('.minus').click(function(){
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        if (oldValue > 1){
            var newValue = parseFloat(oldValue) - 1;
        } else {
            newValue = 1;
        }
        $button.parent().find("input").val(newValue);
        //update summ and summ_total
        cart_row = $(this).closest('.cart-row');
        if (cart_row.length != 0){
            product_id = cart_row.attr('id').replace('cart-product', '');
            change_count_in_cart(product_id, newValue);
        }
    })

   //image carusel
   $('#slider-images-vertical').Thumbelina({
        orientation:'vertical',
        $bwdBut:$('#slider-images-vertical .top'),
        $fwdBut:$('#slider-images-vertical .bottom')
    })

   //image carusel select image  
   $('#slider-images-vertical ul li img').click(function() { 
		var src = $(this).attr("src").match(/[^\.]+/) + ".jpg";
		$('.zoom-section img').attr('src', src);
        $('.zoom-section a').attr('href', src);
        $('a.zoom').easyZoom();
	})
  						
})